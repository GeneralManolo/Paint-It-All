﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ColorChanger : MonoBehaviour
{
    private Renderer render;

    public Material NoColor;
    public Material Player1Color;
    public Material Player2Color;
    public Material Player3Color;
    public Material Player4Color;
    void Start()
    {
        render = GetComponent<Renderer>();
        render.material = NoColor;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player") && gameObject.tag != "Blue")
        {
            gameObject.tag = "Blue";
            render.material = Player1Color;

            if (SceneManager.GetActiveScene().name == "Map1_4Players")
            {
                GameManager.Player1Score += 1;
            }

            if (SceneManager.GetActiveScene().name == "Map2_4Players")
            {
                GameManager.Player1Score += 1;
            }

            if (SceneManager.GetActiveScene().name == "Map1_3Players")
            {
                Player3GameManager.Player1Score += 1;
            }

            if (SceneManager.GetActiveScene().name == "Map2_3Players")
            {
                Player3GameManager.Player1Score += 1;
            }

            if (SceneManager.GetActiveScene().name == "Map1_2Players")
            {
                Player2GameManager.Player1Score += 1;
            }

            if (SceneManager.GetActiveScene().name == "Map2_2Players")
            {
                Player2GameManager.Player1Score += 1;
            }
        }
        
        if (other.gameObject.CompareTag("Player2") && gameObject.tag != "Red")
        {
            gameObject.tag = "Red";
            render.material = Player2Color;

            if (SceneManager.GetActiveScene().name == "Map1_4Players")
            {
                GameManager.Player2Score += 1;
            }

            if (SceneManager.GetActiveScene().name == "Map2_4Players")
            {
                GameManager.Player2Score += 1;
            }

            if (SceneManager.GetActiveScene().name == "Map1_3Players")
            {
                Player3GameManager.Player2Score += 1;
            }

            if (SceneManager.GetActiveScene().name == "Map2_3Players")
            {
                Player3GameManager.Player2Score += 1;
            }

            if (SceneManager.GetActiveScene().name == "Map1_2Players")
            {
                Player2GameManager.Player2Score += 1;
            }

            if (SceneManager.GetActiveScene().name == "Map2_2Players")
            {
                Player2GameManager.Player2Score += 1;
            }
        }

        if (other.gameObject.CompareTag("Player3") && gameObject.tag != "Yellow")
        {
            gameObject.tag = "Yellow";
            render.material = Player3Color;

            if (SceneManager.GetActiveScene().name == "Map1_4Players")
            {
                GameManager.Player3Score += 1;
            }

            if (SceneManager.GetActiveScene().name == "Map2_4Players")
            {
                GameManager.Player3Score += 1;
            }

            if (SceneManager.GetActiveScene().name == "Map1_3Players")
            {
                Player3GameManager.Player3Score += 1;
            }

            if (SceneManager.GetActiveScene().name == "Map2_3Players")
            {
                Player3GameManager.Player3Score += 1;
            }
        }

        if (other.gameObject.CompareTag("Player4") && gameObject.tag != "Green")
        {
            gameObject.tag = "Green";
            render.material = Player4Color;
            GameManager.Player4Score += 1;
        }
    }
}
