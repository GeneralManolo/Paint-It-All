﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuManager : MonoBehaviour
{
    public void PlayTwoPlayers()
    {
        SceneManager.LoadScene("Map1_2Players");
        Time.timeScale = 1;
    }

    public void PlayThreePlayers()
    {
        SceneManager.LoadScene("Map1_3Players");
        Time.timeScale = 1;
    }

    public void PlayFourPlayers()
    {
        SceneManager.LoadScene("Map1_4Players");
        Time.timeScale = 1;
    }

    public void Map2PlayTwoPlayers()
    {
        SceneManager.LoadScene("Map2_2Players");
        Time.timeScale = 1;
    }

    public void Map2PlayThreePlayers()
    {
        SceneManager.LoadScene("Map2_3Players");
        Time.timeScale = 1;
    }

    public void Map2PlayFourPlayers()
    {
        SceneManager.LoadScene("Map2_4Players");
        Time.timeScale = 1;
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("Menu");
        Time.timeScale = 1;
    }

    public void PlayKOTH()
    {
        SceneManager.LoadScene("KOTH_4Players");
    }
    public void Exit()
    {
        Application.Quit();
    }
}
